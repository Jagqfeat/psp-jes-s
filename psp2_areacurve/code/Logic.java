/**
 * Logic.java
 * Ver. 0.8
 * 
 * Fake main from the process, ran by App.java class.
 * Used to call all of the remaining classes in
 *	specific order.
 */

public class Logic {
	/* Creación de Variables */
	private int numSeg;				/* Número de cuadrados */
	private double widthSeg;		/* Tamaño de cuadrados */
	private int degreeFreedom;		/* Máxima libertad */
	private double maxError;		/* Máximo error */
	private double rangeX;			/* Medir de 0 a X */
	private double simpsonValue;	/* Valor final */
	
	/** Corrida de los procesos. */
	public void logic1a() {
		/* Creación de Objetos. */
		Scan objScan = new Scan();
		SimpsonRule objSimpson = new SimpsonRule();
		Output objOutput = new Output();
		
		/*
		 * Instrucciones
		 * Valores constantes
		 * Valores del usuario
		 * Operaciones
		 */
		numSeg = 10;
		maxError = 0.00001;
		 
		System.out.println("Test1: rangeX=1.1 & degreeFreedom=9 \nTest2: rangeX=1.1812 & degreeFreedom=10 \nTest3: rangeX=2.750 & degreeFreedom=30");
		rangeX = objScan.reDouble("rangeX");
		degreeFreedom = objScan.reInt("degreeFreedom");
		
		widthSeg = rangeX / numSeg;
		
		/* LLamar a SimpsonRule para hacer la cadena de métodos. Elegir uno de los dos. */
		simpsonValue = objSimpson.simpTerms(numSeg, degreeFreedom, widthSeg);
		//simpsonValue = objSimpson.calcSimpson(numSeg, degreeFreedom, widthSeg, maxError);
		
		/* Enviar el resultado a Output */
		objOutput.writeData("outFile.txt", simpsonValue);
	}
}