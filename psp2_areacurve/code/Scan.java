/**
 * Scan.java
 * Ver. 0.7
 * 
 * Method to decide which test to produce.
 * Logic consists of two if statements.
 */

import java.util.Scanner;

public class Scan {
	/* Creacion de Variables */
	int wishedValueINT;
	double wishedValueDBL;
	
	/**
	 * Corrida de preguntar que test quiere producir.
	 * PARAMETROS: NONE
	 * RETURN tempValues
	 */
	public int reInt(String name){
		Scanner inputINT = new Scanner(System.in);
		System.out.println("Escribe el valor Integer que deseas para "+name);
		wishedValueINT = inputINT.nextInt();
		return wishedValueINT;
	}
	
	public double reDouble(String name){
		Scanner inputDBL = new Scanner(System.in);
		System.out.println("Escribe el valor Double que deseas para "+name);
		wishedValueDBL = inputDBL.nextDouble();
		return wishedValueDBL;
	}
}