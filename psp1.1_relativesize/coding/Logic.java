/**
 * Logic.java
 * Ver. 0.7
 * 
 * Fake main from the process, ran by App.java class.
 * Used to run all of the remaining classes in
 *	specific order.
 */

public class Logic {
	/* Creación de Variables */
	private String tempData;		/* Texto del archivo. */
	private String tempValues;		/* Decidir entre los dos arreglos. */
	private double[] arrValues;		/* Almacenaje de los valores. */
	private int intNum;				/* Número de elementos en arrValues. */
	private double[] arrLogN;			/* Valor final. */
	
	/** Corrida de los procesos. */
	public void logic1a() {
		/* Creación de Objetos. */
		Scan objScan = new Scan();
		Input objInput = new Input();
		Data objData = new Data();
		Media objMedia = new Media();
		RelativeSize objRelative = new RelativeSize();
		Output objOutput = new Output();
		
		/* Archivo -> String -> String[] */
		tempValues = objScan.decideTest();
			tempData = objInput.readData(tempValues);
		arrValues = objData.saveData(tempData);

		/* Obtener el número de valores en arrValues. */
		intNum = arrValues.length;
		
		/* LLamar a RelativeSize para hacer la cadena de métodos. */
		arrLogN = objRelative.getLogRange(arrValues, intNum);
		
		/* Enviar el resultado a Output */
		objOutput.writeData("outFile.txt", arrLogN);
	}
}