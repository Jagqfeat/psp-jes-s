/*
* Output.java
* Ver. 0.5
* 
* Static void main of the psp0.1_linesofcode process.
* Used to run Logic.java, with no parameters.
*/

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Output {
	/* Creación de variables. */
	BufferedWriter output = null;		/* Objeto de escritura. */
	String textOut;						/* Texto a escribir. */
	
	/**
	 * Corrida de escritura en el archivo de salida.
	 * PARAMETROS: outFile, arrLogN
	 * RETURN: Nothing, void class.
	 */
	public void writeData(String outFile, double[] arrLogN) {
		/* Try catch para evitar que no haya qué escribir. */
		textOut = (	"El valor de VS fue "+arrLogN[0]+" ---- "+
					"El valor de S fue " +arrLogN[1]+" ---- "+
					"El valor de M fue " +arrLogN[2]+" ---- "+
					"El valor de L fue " +arrLogN[3]+" ---- "+
					"El valor de VL fue "+arrLogN[4]+" ---- " );
		try {
			File file = new File(outFile);
			output = new BufferedWriter(new FileWriter(file));
			output.write(textOut);
			output.close();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
}