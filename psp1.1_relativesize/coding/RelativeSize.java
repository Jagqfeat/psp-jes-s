/**
 * RelativeSize.java
 * Ver 0.51
 * Created from EstimateCorrelation.java
 *
 * Independent process to calculate the range between VerySmall to
 *		VeryLarge with log functions.
 * The ultimate process calls out the minor ones.
 */
 
 public class RelativeSize{
	 
	/** Obtain the Average of the array. */
	public double getAvg(double[] arrValues, int intNum){
		Media objMedia = new Media();
		
		double dblAvg;
		dblAvg = objMedia.getMedia(arrValues, intNum);
		return dblAvg;
	}

	/** Obtain Variation with the average. */
	public double getVar(double[] arrValues, int intNum){
		RelativeSize objRelative = new RelativeSize();
		
		double dblVar = 0;
		double dblAvg = objRelative.getAvg(arrValues, intNum);
		
		for (int i=0; i<intNum; i++)
			dblVar += Math.pow( (Math.log(arrValues[i]) - dblAvg) , 2 );
		
		dblVar = dblVar / (intNum-1);
		return dblVar;
	}
	
	/** Obtain Desviation Standard from the variation. */
	public double getDev(double[] arrValues, int intNum){
		RelativeSize objRelative = new RelativeSize();
		
		double dblDev;
		double dblVar = objRelative.getVar(arrValues, intNum);
		dblDev = Math.pow(dblVar,.5);
		return dblDev;
	}
	
	/** Obtain the Variation between VerySmall and VeryLarge. */
	public double[] getLogRange(double[] arrValues, int intNum){
		RelativeSize objRelative = new RelativeSize();
		
		double[] arrLogN = new double[5];
		double dblAvg = objRelative.getAvg(arrValues, intNum);
		double dblDev = objRelative.getDev(arrValues, intNum);
		
		arrLogN[0] = Math.exp( dblAvg - 2*dblDev );
		arrLogN[1] = Math.exp( dblAvg - dblDev );
		arrLogN[2] = Math.exp( dblAvg );
		arrLogN[3] = Math.exp( dblAvg + dblDev );
		arrLogN[4] = Math.exp( dblAvg + 2*dblDev );
		
		return arrLogN;
	}
 }