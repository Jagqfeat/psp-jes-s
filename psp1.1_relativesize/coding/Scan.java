/**
 * Scan.java
 * Ver. 0.6
 * 
 * Method to decide which test to produce.
 * Logic consists of two if statements.
 */

import java.util.Scanner;

public class Scan {
	/* Creaci�n de Variables */
	int wishedValues;
	String tempValues;
	
	/**
	 * Corrida de preguntar qu� test quiere producir.
	 * PARAMETROS: NONE
	 * RETURN tempValues
	 */
	public String decideTest(){
		Scanner input = new Scanner(System.in);
		System.out.println("Escribe el n�mero que deseas para probar. ");
		System.out.println("Test 1: Elige 1. \nTest 2: Elige 2.");
		wishedValues = input.nextInt();
		
		switch (wishedValues){
			case 1:
				tempValues = "inValuesLOCs.txt";
				break;
			case 2:
				tempValues = "inValuesBOOKs.txt";
				break;
			default:
				System.out.println("No se ingres� un valor v�lido. \nSaliendo del sistema.");
				System.exit(0);
		}
		
		return tempValues;
	}
}