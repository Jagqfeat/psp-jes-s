public class Media {

		// Creación de variables.
	private double media;
	private double sumatoria = 0;

	/**
	 * 
	 * @param arrayData
	 */
	public double getMedia(double[] arrayData, int n) {
		// return this.media;
		
		// Generar la media.
	// Sumatoria de las variables en arrayData.
	for (int i = 0; i < n; i++)
		sumatoria += arrayData[i];
	// División entre la cantidad de elementos.
	media = sumatoria / n;
	
		// Regresar la media a Logic.java
	return media;
	}

}