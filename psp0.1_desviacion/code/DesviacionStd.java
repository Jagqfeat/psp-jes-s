public class DesviacionStd {

		// Creación de Variables.
	private double desv;
	private double sumatoria = 0;

	/**
	 * 
	 * @param arrayData
	 * @param media
	 */
	public double getDesviacion(double[] arrayData, double media, int n) {
		// TODO - implement DesviacionStd.getDesviacion
		// throw new UnsupportedOperationException();
		
		// Generar la desviación.
	// La Sumatoria (Xi-X)2
	for (int i = 0; i < n; i++)
		sumatoria += Math.pow((arrayData[i] - media),2);
	// División sobre n y raíz.
	sumatoria = sumatoria / (n-1);
	desv = Math.sqrt(sumatoria);
	
		// Regresar la desviacion a Logic.java
	return desv;
	}

}