// Importe de Scanner.
import java.util.Scanner;

public class Input {
	
	/**
	 * 
	 * @param arrayData
	 */
	public double[] readKeyboard(double[] arrayData, int n) {
		// TODO - implement Input.readKeyboard
		// throw new UnsupportedOperationException();

		// Generación de objeto Scanner double.
	Scanner input = new Scanner(System.in);
		
		// Obtener valores y asignar desde el teclado.
	for (int i = 0; i < n; i++){
		System.out.println("Inserte el valor para "+i);
		arrayData[i] = input.nextDouble();
	}

		// Regresar arrayData de vuelta a Logic.java
	return arrayData;
	}
}