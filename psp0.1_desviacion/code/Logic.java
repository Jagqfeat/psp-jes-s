public class Logic {

		// Creaci�n de variables.
	private double media;
	private double desv;
	private double[] arrayData;
	private int n;

	public void logic1() {
		// TODO - implement Logic.logic1
		// throw new UnsupportedOperationException();


		// Creaci�n de objetos con las clases.
	Data objData = new Data();
	Input objInput = new Input();
	Media objMedia = new Media();
	DesviacionStd objDesviacion = new DesviacionStd();
	Output objOutput = new Output();
	
		// Run de las clases en orden.
		
	// Asignar a valor nulo "n" la cantidad de variables.
	n = objData.saveData(n);
	// Generar un arreglo del tama�o de n, y asignar valores.
	arrayData = new double[n];
	arrayData = objInput.readKeyboard(arrayData, n);
	// Utilizar arrayData y n para hacer una media.
	media = objMedia.getMedia(arrayData, n);
	// Utilizar arrayData, media, y n para hacer una desviaci�n.
	desv = objDesviacion.getDesviacion(arrayData, media, n);
	// Utilizar desv, media, y n para presentar a la pantalla.
	objOutput.printResults(desv, media, n);
	}

}