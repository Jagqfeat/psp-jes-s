/*
* Output.java
* Ver. 0.5
* 
* Static void main of the psp0.1_linesofcode process.
* Used to run Logic.java, with no parameters.
*/

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Output {
	/* Creación de variables. */
	BufferedWriter output = null;		/* Objeto de escritura. */
	String textOut;						/* Texto a escribir. */
	
	/**
	 * Corrida de escritura en el archivo de salida.
	 * PARAMETROS: inFile, outFile
	 * RETURN: Nothing, void class.
	 */
	public void writeData(String outFile, double dblB1, double dblB0, double dblR, double dblYk) {
		/* Try catch para evitar que no haya qué escribir. */
		textOut = (	"El valor de B1 fue "+dblB1+" ---- "+
					"El valor de B0 fue "+dblB0+" ---- "+
					/* "El valor de R fue "+dblR+" ---- "+ */
					"El valor de Yk fue "+dblYk);
		try {
			File file = new File(outFile);
			output = new BufferedWriter(new FileWriter(file));
			output.write(textOut);
			output.close();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
}