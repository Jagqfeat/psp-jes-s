/**
* Data.java
* Ver. 0.6
* 
* Creation of the array for further use.
* Condition of each array position set by the commas.
*/
public class Data {
	
	/**
	 * Separación de cada línea en una posición de arreglo.
	 * PARAMETROS: data
	 * RETURN: arrTemp
	 */
	public double[] saveData(String tempData) {
		/* Split del contenido y devolución. CONDICIÓN: "," */
		String[] arrTempSTR = tempData.split(",");
		double[] arrTempDBL = new double[arrTempSTR.length];
		for (int i = 0; i < arrTempSTR.length; i++){
			arrTempDBL[i] = Double.parseDouble(arrTempSTR[i]);
		}
		return arrTempDBL;
	}
}