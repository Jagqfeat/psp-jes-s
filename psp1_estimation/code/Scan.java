/**
 * Scan.java
 * Ver. 0.5
 * 
 * Method to decide which test to produce.
 * Logic consists of two if statements.
 */

import java.util.Scanner;

public class Scan {
	/* Creaci�n de Variables */
	int wishedColumn;
	String tempColumn;
	
	/**
	 * Corrida de preguntar qu� test quiere producir.
	 * PARAMETROS TimesRan
	 * RETURN int
	 */
	public String decideTest(int currentTest){
		Scanner input = new Scanner(System.in);
		System.out.println("Escribe el nombre del texto que deseas para el arreglo "+currentTest);
		System.out.println(" Test1 arrX=1 arrY=3 \n Test2 arrX=1 arrY=4 \n Test3 arrX=2 arrY=3 \n Test4 arrX=2 arrY=4");
		wishedColumn = input.nextInt();
		
		switch (wishedColumn){
			case 1:
				tempColumn = "Values1.txt";
				break;
			case 2:
				tempColumn = "Values2.txt";
				break;
			case 3:
				tempColumn = "Values3.txt";
				break;
			case 4:
				tempColumn = "Values4.txt";
				break;
			default:
				System.out.println("No se ingres� un valor v�lido. \nSaliendo del sistema.");
				System.exit(0);
		}
		
		return tempColumn;
	}
}