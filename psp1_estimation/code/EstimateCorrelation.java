/**
 * EstimateCorrelation.java
 * Ver 0.5
 *
 * Independent process to calculate R1, B1, B0, and Yk from given
 * 		values in parameters.
 * Each ultimate process (stated above) calls out the minor ones.
 */
 
 public class EstimateCorrelation{
	 
	/** 
	 * Lista de procesos.
	 * Sumas de X, Y, XX, XY, XY.
	 */
	public double sumX(double[] arrX, int intNum){
		double dblSumX = 0;
		for (int i=0; i<intNum; i++)
			dblSumX += arrX[i];
		return dblSumX;
	}

	public double sumY(double[] arrY, int intNum){
		double dblSumY = 0;
		for (int i=0; i<intNum; i++)
			dblSumY += arrY[i];
		return dblSumY;
	}

	public double sumXX(double[] arrX, int intNum){
		double dblSumXX = 0;
		for (int i=0; i<intNum; i++)
			dblSumXX += Math.pow(arrX[i],2);
		return dblSumXX;
	}

	public double sumXY(double[] arrX, double[] arrY, int intNum){
		double dblSumXY = 0;
		for (int i=0; i<intNum; i++)
			dblSumXY += ( arrX[i] * arrY[i] );
		return dblSumXY;
	}

	public double sumYY(double[] arrY, int intNum){
		double dblSumYY = 0;
		for (int i=0; i<intNum; i++)
			dblSumYY += Math.pow(arrY[i],2);
		return dblSumYY;
	}

	/** 
	 * Lista de procesos.
	 * Medias de X, Y.
	 */
	public double avgX(double[] arrX, int intNum){
		double dblAvgX;
		Media objMedia = new Media();
			dblAvgX = objMedia.getMedia(arrX, intNum);
		return dblAvgX;
	}

	public double avgY(double[] arrY, int intNum){
		double dblAvgY;
		Media objMedia = new Media();
			dblAvgY = objMedia.getMedia(arrY, intNum);
		return dblAvgY;
	}
	
	/**
	 * Lista de procesos
	 * Obtener R, B1, B0, Yk.
	 */
	 
	public double getB1(double[] arrX, double[] arrY, int intNum){
		double dblB1;
		EstimateCorrelation objEstimate = new EstimateCorrelation();
		
		double dblSumXY = objEstimate.sumXY(arrX, arrY, intNum);
		double dblAvgX = objEstimate.avgX(arrX, intNum);
		double dblAvgY = objEstimate.avgY(arrY, intNum);
		double dblSumXX = objEstimate.sumXX(arrX, intNum);
		
		dblB1 = ( dblSumXY - intNum*(dblAvgX * dblAvgY) );					
		dblB1 = ( dblB1 / (dblSumXX - intNum*Math.pow(dblAvgX,2)) );
		return dblB1;
	}
	
	public double getB0(double[] arrX, double[] arrY, int intNum){
		double dblB0;
		EstimateCorrelation objEstimate = new EstimateCorrelation();
		
		double dblAvgY = objEstimate.avgY(arrY, intNum);
		double dblB1 = objEstimate.getB1(arrX, arrY, intNum);
		double dblAvgX = objEstimate.avgX(arrX, intNum);
		
		dblB0 = ( dblAvgY - (dblB1 * dblAvgX) );
		return dblB0;
	}
	
	public double getR(double[] arrX, double[] arrY, int intNum){
		double dblR;
		EstimateCorrelation objEstimate = new EstimateCorrelation();
		
		double dblSumXY = objEstimate.sumXY(arrX, arrY, intNum);
		double dblSumX = objEstimate.sumX(arrX, intNum);
		double dblSumY = objEstimate.sumY(arrY, intNum);
		double dblSumXX = objEstimate.sumXX(arrX, intNum);
		double dblSumYY = objEstimate.sumYY(arrY, intNum);
		
		dblR = ( intNum*(dblSumXY) - (dblSumX * dblSumY) );
		double tempDivision = ( (intNum*(dblSumXX) - dblSumX) * (intNum*(dblSumYY) - dblSumY) );
		dblR = ( dblR / Math.pow(tempDivision,.5) );
		return dblR;
	}
	
	public double getYk(double[] arrX, double[] arrY, int intNum){
		double dblYk;
		EstimateCorrelation objEstimate = new EstimateCorrelation();
		
		double dblB0 = objEstimate.getB0(arrX, arrY, intNum);
		double dblB1 = objEstimate.getB1(arrX, arrY, intNum);
		int intXk = 386;
		
		dblYk = ( dblB0 + (dblB1*intXk) );
		return dblYk;
	}
 }