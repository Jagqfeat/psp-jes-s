/**
 * Logic.java
 * Ver. 0.6
 * 
 * Fake main from the process, ran by App.java class.
 * Used to run all of the remaining classes in
 *	specific order.
 */

public class Logic {
	/* Creación de Variables */
	private String tempData;	/* Texto del archivo. */
	private String tempColumn;		/* Decidir que arreglos se utilizaran*/
	private double[] arrX;			/* Almacenaje de valores en X */
	private double[] arrY;
	private int intNum;			/* Número de elementos en el arrX o arrY */
	private int numExec;		/* Número de ejecutables. */
	private double dblB1;
	private double dblB0;
	private double dblR;
	private double dblYk;
	
	/** Corrida de los procesos. */
	public void logic1a() {
		/* Creación de Objetos. */
		Scan objScan = new Scan();
		Input objInput = new Input();
		Data objData = new Data();
		Media objMedia = new Media();
		EstimateCorrelation objEstimate = new EstimateCorrelation();
		Output objOutput = new Output();
		
		/*
		 * Archivo -> String -> String[] 
		 * Se procesa 2 veces, una para cada columna.
		 */
		tempColumn = objScan.decideTest(1);
		tempData = objInput.readData(tempColumn);
			arrX = objData.saveData(tempData);
			
		tempColumn = objScan.decideTest(2);
		tempData = objInput.readData(tempColumn);
			arrY = objData.saveData(tempData);

		/* Obtener el número de valores en arrX y arrY. */
		intNum = arrX.length;
		
		/* LLamar a EstimateCorrelation para hacer la cadena de métodos. */
		dblB1 = objEstimate.getB1(arrX, arrY, intNum);
		dblB0 = objEstimate.getB0(arrX, arrY, intNum);
		dblR = objEstimate.getR(arrX, arrY, intNum);
		dblYk = objEstimate.getYk(arrX, arrY, intNum);
		
		/* Enviar el resultado a Output */
		objOutput.writeData("outFile.txt", dblB1, dblB0, dblR, dblYk);
	}
}