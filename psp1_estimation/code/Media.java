/**
 * Media.java
 * Ver 0.6
 *
 * Reused class in order to create a Media from given
 * 		values in parameters.
 * Modified to keep in touch with modern coding standards.
 */

public class Media {

	/* Creación de variables. */
	private double dblMedia = 0;

	/**
	 * Getting Media
	 * PARAMETERS: arrData, intNum;
	 * RETURN: mediaData
	 */
	public double getMedia(double[] arrData, int intNum) {

	for (int i=0; i<intNum; i++)
		dblMedia += arrData[i];
	dblMedia = dblMedia / intNum;
	
	return dblMedia;
	}
}