/**
 * SimpsonRule.java
 * Ver 0.52
 * Created from RelativeSize.java
 *
 * Independent process to calculate the area below the curve
 * 		with the use of the Simpson's rule. This consists of using
 *		small squares and calculating its area separately.
 * The ultimate process calls out the minor ones.
 */
 
 public class SimpsonRule{	
	/**
	 * Obtain Gamma 1.
	 * Operation: (degreeFreedom + 1) / 2
	 * Creates double gamma1;
	 */
	public double getGamma1(int degreeFreedom){
		/* Se hace un double dado que un int no regresa decimales al dividirse. */
		double degreeFreedomDBL = degreeFreedom;
		
		double gamma1;
		
		gamma1 = (degreeFreedomDBL + 1) / 2;
		
		return gamma1;
	}

	/**
	* Obtain Gamma 2.
	* Operation: degreeFreedom / 2
	* Creates double gamma2;
	*/
	public double getGamma2(int degreeFreedom){
		/* Se hace un double dado que un int no regresa decimales al dividirse. */
		double degreeFreedomDBL = degreeFreedom;
		
		double gamma2;
		
		gamma2 = degreeFreedomDBL / 2;
		
		return gamma2;
	}
	
	/**
	 * Obtain Factorial
	 * Operation: Factorial of Gamma 1
	 * Creates double factorialValue;
	 * NOTE: Factorial of .5 is SQRT(Pi)
	 */
	public double getFactorial(int degreeFreedom){
		SimpsonRule objSimpson = new SimpsonRule();
		double gamma1 = objSimpson.getGamma1(degreeFreedom);
		
		/* La formula de Factorial se multiplica, asi que el valor inicial es 1. */
		double factorialValue = 1;
		
		for (double i=(gamma1 -1); i>0; i--){
			if (i == .5){
				factorialValue *= i;
				factorialValue *= Math.pow(Math.PI,.5);
			} else {
				factorialValue *= i;
			}
		}

		return factorialValue;
	}
	
	/**
	 * Obtain RealValue
	 * Operation: Factorial of Gamma 2
	 * Creates double realValue;
	 * NOTE: Factorial of .5 is SQRT(Pi)
	 */
	public double getRealValue(int degreeFreedom){
		SimpsonRule objSimpson = new SimpsonRule();
		double gamma2 = objSimpson.getGamma2(degreeFreedom);
		
		/* La formula de Factorial se multiplica, asi que el valor inicial es 1. */
		double realValue = 1;
		
		for (double i=(gamma2 -1); i>0; i--){
			if (i == .5){
				realValue *= i;
				realValue *= Math.pow(Math.PI,.5);
			} else {
				realValue *= i;
			}
		}
		
		return realValue;
	}
	
	/**
	 * Obtain array of Xis
	 * Operation: i * widthSeg
	 * Creates double[] arrXi;
	 */
	public double[] getarrXi(int numSeg, double widthSeg){
		/* Arreglo de 0 a 10, Dando un total de 11 valores. (numSeg+1) */
		double[] arrXi = new double[numSeg+1];
		
		for (int i=0; i<(numSeg+1); i++)
			arrXi[i] = i * widthSeg;
		
		return arrXi;
	}
	
	/**
	 * Obtain array of Multiplier.
	 * Operation: First and Last value are set to 1.
	 * 		The rest are alternates of 4 and 2.
	 * Creates int[] multiplier;
	 */
	public int[] getMultiplier(int numSeg){
		/* Arreglo de 0 a 10, Dando un total de 11 valores. (numSeg+1) */
		int[] multiplier = new int[numSeg+1];
		
		/* El primer [0] y último valor [10] es 1. */
		multiplier[0] = 1;
		multiplier[numSeg] = 1;
		
		/*
		 * Ciclo for empezando de [1] a [9]
		 * i=1; i<numSeg
		 */
		for (int i=1; i<numSeg; i++){
			if ( (i%2)==0 ){
				multiplier[i] = 2;
			} else {
				multiplier[i] = 4;
			}
		}
		
		return multiplier;
	}
	
	/**
	 * Obtain First Operation of the Simpson's Rule.
	 * Operation: 1 + ( Math.pow(arrXi[i],2) / degreeFreedom )
	 * Creates double[] simpF1;
	 */
	public double[] simpF1(int numSeg, int degreeFreedom, double widthSeg){
		SimpsonRule objSimpson = new SimpsonRule();
		double[] arrXi = objSimpson.getarrXi(numSeg, widthSeg);
		double[] simpF1 = new double[numSeg+1];
		
		for (int i=0; i<(numSeg+1); i++)
			simpF1[i] = 1 + ( Math.pow(arrXi[i],2) / degreeFreedom );
			
		return simpF1;
	}
	
	/**
	 * Obtain Second Operation of the Simpson's Rule.
	 * Operation: Math.pow( simpF1[i] , (-1*gamma1) )
	 * Creates double[] simpF2;
	 */
	public double[] simpF2(int numSeg, int degreeFreedom, double widthSeg){
		SimpsonRule objSimpson = new SimpsonRule();
		double gamma1 = objSimpson.getGamma1(degreeFreedom);
		double[] simpF1 = objSimpson.simpF1(numSeg, degreeFreedom, widthSeg);
		
		/* Arreglo de 0 a 10, Dando un total de 11 valores. (numSeg+1) */		
		double[] simpF2 = new double[numSeg+1];
		
		for (int i=0; i<(numSeg+1); i++)
			simpF2[i] = Math.pow ( simpF1[i] , (-1*gamma1) );
			
		return simpF2;
	}
	
	/**
	 * Obtain Third Operation (Gamma) of the Simpson's Rule.
	 * Operation: factorialValue / ( Math.SQRT(degreeFreedom*Math.PI) * realValue )
	 * Creates double simpG;
	 */
	public double simpG(int numSeg, int degreeFreedom){
		SimpsonRule objSimpson = new SimpsonRule();
		double factorialValue = objSimpson.getFactorial(degreeFreedom);
		double realValue = objSimpson.getRealValue(degreeFreedom);
		
		double simpG;
		
		simpG = factorialValue / ( Math.sqrt(degreeFreedom*Math.PI) * realValue );

		return simpG;
	}
	
	/**
	 * Obtain Fourth Operation (Fx) of the Simpson's Rule.
	 * Operation: simpG * simpF2[i]
	 * Creates double[] simpFx;
	 */
	public double[] simpFx(int numSeg, int degreeFreedom, double widthSeg){
		SimpsonRule objSimpson = new SimpsonRule();
		double simpG = objSimpson.simpG(numSeg, degreeFreedom);
		double[] simpF2 = objSimpson.simpF2(numSeg, degreeFreedom, widthSeg);
		
		/* Arreglo de 0 a 10, Dando un total de 11 valores. (numSeg+1) */		
		double[] simpFx = new double[numSeg+1];
		
		for (int i=0; i<(numSeg+1); i++)
			simpFx[i] = simpG * simpF2[i];

		return simpFx;
	}
	
	/**
	 * Obtain Last Operation (Terms) of the Simspon's Rule.
	 * Operation: (widthSeg/3) * multiplier[i] * simpFx[i];
	 * Creates double finalValue;
	 */
	public double simpTerms(int numSeg, int degreeFreedom, double widthSeg){
		SimpsonRule objSimpson = new SimpsonRule();
		int[] multiplier = objSimpson.getMultiplier(numSeg);
		double[] simpFx = objSimpson.simpFx(numSeg, degreeFreedom, widthSeg);
		
		/* Arreglo de 0 a 10, Dando un total de 11 valores. (numSeg+1) */				
		double[] simpTerms = new double[numSeg+1];
		double finalValue = 0;
		
		for (int i=0; i<(numSeg+1); i++)
			simpTerms[i] = (widthSeg/3) * multiplier[i] * simpFx[i];
		
		for (int i=0; i<(numSeg+1); i++)
			finalValue += simpTerms[i];
				
		return finalValue;
	}
	
	/**
	 * Run the Simpson's Rule operations in order.
	 * NOTE: Using the maxError, the program should run its values with
	 * 		higher numSegments until it is acceptable.
	 *
	 * 		NOTA: NO SE UTILIZA POR EL MOMENTO.
	 *
	 */
	public double calcSimpson(int numSeg, int degreeFreedom, double widthSeg, double maxError){
		SimpsonRule objSimpson = new SimpsonRule();
		int numberOfTries = 0;
		boolean correctError = false;
		double tempTry1 = 0;
		double tempTry2 = 0;
		double currentError = 0;
		double finalValue = 0;
		
		do{
			if (numberOfTries % 2 == 0)
				tempTry1 = objSimpson.simpTerms(numSeg, degreeFreedom, widthSeg);
			else 
				tempTry2 = objSimpson.simpTerms(numSeg, degreeFreedom, widthSeg);
			
			if (numberOfTries >= 1){
				if (tempTry1 > tempTry2){
					currentError = tempTry1 - tempTry2;
					if (currentError < maxError){
						correctError = true;
						finalValue = tempTry1;
					}
					return tempTry1;
				} else {
					currentError = tempTry2 - tempTry1;
					if (currentError < maxError){
						correctError = true;
						finalValue = tempTry2;
					}
					return tempTry2;
				}	
			}
			
			numSeg *= 2;
			numberOfTries ++;
			currentError = 0;
		} while (correctError == false);
		
		return finalValue;
	}

}