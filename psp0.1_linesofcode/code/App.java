/*
* App.java
* Ver. 0.5
* 
* Static void main of the psp0.1_linesofcode process.
* Used to run Logic.java, with no parameters.
*/

public class App {
	
	/** Corrida del main. */
	public static void main(String[] args) {
		
	/* Crear un objeto y correr logic1a de Logic.java */
	Logic mylogic1a = new Logic();
	mylogic1a.logic1a();
	}
}