/*
* Data.java
* Ver. 0.5
* 
* Creation of the array for further use.
* Condition of each array position set by the commas.
*/
public class Data {
	
	/**
	 * Separación de cada línea en una posición de arreglo.
	 * PARAMETROS: data
	 * RETURN: arrData
	 */
	public String[] saveData(String data) {
		/* Split del contenido y devolución. CONDICIÓN: "," */
		String[] arrData = data.split("¡");
		return arrData;
	}
}