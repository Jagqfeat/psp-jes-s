/*
* SearchExec.java
* Ver. 0.5
* 
* Process to find the amount of executables per archive.
* Using if statements to remove comments, spaces, etc,
* 	to find and then nuliffy them as a line alltogether.
*/

public class SearchExec {
	/* Creaci�n de Variables */
	int numExec = 0;		/* N�mero de keywords Executables. */
	String tempData;		/* Toma el String de arrData[i] para su trim. */ 
	
	/**
	 * Corrida de busqueda de lineas no validas.
	 * PARAMETROS: arrData
	 * RETURN: numExec
	 */
	public int getExec(String[] arrData) {
		/*
		 * Se utilizara un for para leer cada l�nea de codigo.
		 * Se copia el String arrData[i] a tempData para
		 *		aplicar la funcion tempData.trim(), se sume numExec
		 * 		y se anule en caso de ser comentario, espacio, etc.
		 */
		for (int i = 0; i < arrData.length; i ++){
			tempData = arrData[i];
			tempData = tempData.trim();
			
			if (
				( tempData.startsWith("/*") == true ) ||
				( tempData.startsWith("*") == true ) ||
				( tempData.startsWith("*/") == true ) ||
				( tempData.startsWith("//") == true) ||
				( tempData.equals("") == true)
			){
				continue;
			} else numExec ++;
			
			tempData = "";
		}
		/* Regresa la cantidad de numExec. */
		return numExec;
	}
}