/*
* Input.java
* Ver. 0.5
* 
* Reader of the inFile and conversion of each line
*	 with a comma in order to separate it on arrays later.
* Usage of try catch.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Input {
	/* Creación de Variables */
	String data;					/* Almacenaje de los datos del archivo. */
	BufferedReader br = null;		/* Objeto de Lectura. */
	
	/**
	 * Corrida de leer el archivo.
	 * PARAMETROS: inFile
	 * RETURN: data
	 */
	public String readData(String inFile) {
		/* 
		 * Try catch para evitar lectura de archivos vacíos.
		 * Se separa cada línea con "," para su lectura posterior.
		 */ 
		try {
			br = new BufferedReader(new FileReader(inFile));
			StringBuilder sb = new StringBuilder();
			String line;			/* String para tomar el texto temporalmente. */
			while ( (line = br.readLine()) != null ) {
				sb.append(line);
				sb.append("¡");
			}
			data = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/* Regresa un string con el contenido del archivo. */
		return data;
	}
}