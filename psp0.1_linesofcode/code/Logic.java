/*
 * Logic.java
 * Ver. 0.5
 * 
 * Fake main from the process, ran by App.java class.
 * Used to run all of the remaining classes in
 *	specific order.
 */

public class Logic {
	/* Creación de Variables */
	private String data;		/* Texto del archivo. */
	private String[] arrData;	/* Almacenaje del texto del archivo por línea. */
	private int numExec;		/* Número de ejecutables. */
	
	/** Corrida de los procesos. */
	public void logic1a() {
		/* Creación de Objetos. */
		Input objInput = new Input();
		Data objData = new Data();
		SearchExec objExec = new SearchExec();
		Output objOutput = new Output();
		
		/* Procesado del texto. Archivo -> String -> String[] */
		data = objInput.readData("inFileDESV.java");
		arrData = objData.saveData(data);
		
		/* Obtener las líneas de código. */
		numExec = objExec.getExec(arrData);
		
		/* Enviar el resultado a Output */
		objOutput.writeData("inFileDESV.java", "outFileDESV.txt", numExec, arrData);
	}
}