/**
* App.java
* Ver. 0.9
* 
* Static void main of the psp2.1_gausequation process.
* Migrated from the psp1_estimation project.
* Used to run Logic.java, with no parameters.
*/
public class App {
	
	/** Corrida del main. */
	public static void main(String[] args) {
		
	/* Crear un objeto y correr logic1a de Logic.java */
	Logic myLogic1a = new Logic();
	myLogic1a.logic1a();
	
	}
}