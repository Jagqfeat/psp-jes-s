/**
* Output.java
* Ver. 0.8
* 
* Static void main of the psp0.1_linesofcode process.
* Used to run Logic.java, with no parameters.
*/

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Output {
	/* Creación de variables. */
	BufferedWriter output = null;		/* Objeto de escritura. */
	String textOut;						/* Texto a escribir. */
	
	/**
	 * Corrida de escritura en el archivo de salida.
	 * PARAMETROS: outFile, simpsonValue
	 * RETURN: Nothing, void class.
	 */
	public void writeData(String outFile, double expectedValue, double upperLimit) {
		/* Try catch para evitar que no haya qué escribir. */
		textOut = ("El límite requerido para obtener "+expectedValue+" fue de "+upperLimit+" ¡Gracias por el semestre prof. Centeno!");
		try {
			File file = new File(outFile);
			output = new BufferedWriter(new FileWriter(file));
			output.write(textOut);
			output.close();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
}