/**
 * GausEquation.java
 * Ver. 0.5
 *
 * Major process to call out SimpsonRule integration
 * 		and force it to find a specified value by
 * 		manipulating the upper limit.
 */

public class GausEquation {
	
	/* Creaci�n de Variables */
	private double tempIntegration;			/* Toma temporalmente la integraci�n generada con upperLimit. */
	private double difference;				/* La diferencia CON signo de expectedValue - tempIntegration */
	private double differenceABS;			/* La diferencia absoluta de expectedValue - tempIntegration */
	private int numRuns = 1;				/* Conteo de procesos whiles. */
	private char currentSign;				/* Signo que determina si la diferencia es negativa o positiva. */
	private boolean errorCorrect = false;	/* Condici�n para salir. */
	
	/** Begin calling SimpsonRule and changing the upperLimit to find an exact value. */
	public double callSimpsonRule(int numSeg, double maxError, double upperLimit, double adjustment, int degreeFreedom, double expectedValue, double widthSeg){

		/* Creaci�n de Objetos */
		SimpsonRule objSimpson = new SimpsonRule();
		
		/* Realizar un while errorCorrect sea falso. */
		while (errorCorrect == false){
			
			widthSeg = (upperLimit / numSeg);
			
			tempIntegration = objSimpson.simpTerms(numSeg, degreeFreedom, widthSeg);
			difference = (expectedValue - tempIntegration);
			differenceABS = Math.abs(difference);
			
			/* Dos ifs iniciales. 1�Determinar signo durante la primera corrida, 2�Salir prematuramente si se obtuvo errorCorrect. */
			if (numRuns == 1){
				if (difference < 0) currentSign = '-';
				else currentSign = '+';
			}
			if (differenceABS <= maxError){
				/**/ System.out.println("La integraci�n obtenida fue: "+tempIntegration);
				errorCorrect = true;
				break;
			}
			
			/* Four ifs for each case: 1�Positive to Negative, 2�Negative to Positive, 3�Too Low, 4�Too High */
			if (currentSign == '+'){
				if (difference < 0){
					currentSign = '-';
					adjustment /= 2;
					System.out.println("d = "+adjustment);
				}
			}
			if (currentSign == '-'){
				if (difference > 0){
					currentSign = '+';
					adjustment /= 2;
					System.out.println("d = "+adjustment);
				}
			}
			if (tempIntegration < expectedValue){
				upperLimit += adjustment;
				System.out.println("x = "+upperLimit);
			}
			if (tempIntegration > expectedValue){
				upperLimit -= adjustment;
				System.out.println("x = "+upperLimit);
			}
			numRuns ++;
		}
		System.out.println("\nLa obtenci�n fue satisfactoria en "+numRuns+" intentos.\nSu l�mite superior lo encontrar� en outCurrentTest.txt\n�Feliz verano!");
		return upperLimit;
	}
}