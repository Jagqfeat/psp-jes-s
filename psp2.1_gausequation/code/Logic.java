/**
 * Logic.java
 * Ver. 1.0
 * 
 * Fake main from the process, ran by App.java class.
 * Used to call all of the remaining classes in
 *	specific order.
 */

public class Logic {
	
	/* Creación de Variables */
	private int numSeg;				/* Número de cuadrados */
	private double maxError;		/* Error maximo */
	private double upperLimit;		/* Límites de integracion, de 0 a X */
	private double adjustment;		/* Valor en el que el límite superior se cambia. */
	private int degreeFreedom;		/* Libertad maxima */
	private double expectedValue;	/* Valor al que se desea llegar */
	private double widthSeg;		/* Tamaño de cuadrados */
	
	/** Corrida de los procesos. */
	public void logic1a() {
		
		/* Creación de Objetos. */
		Scan objScan = new Scan();
		GausEquation objGaus = new GausEquation();
		Output objOutput = new Output();
		
		/* 
		 * numSeg, maxError, upperLimit, adjustment : Constantes en Logic.java
		 * degreeFreedom, expectedValues : Ingreso por Usuario en Scan.java
		 * upperLimit, adjustment, widthSeg : Procesos en GausEquation.java
		 */
		numSeg = 10;
		maxError = 0.00001;
		upperLimit = 1.0;
		adjustment = 0.5;
		
		System.out.println("Test1: degreeFreedom=6, expectedValue=0.20\nTest2: degreeFreedom=15, expectedValue=0.45\nTest3: degreeFreedom:4, expectedValue:0.495");
		degreeFreedom = objScan.reInt("degreeFreedom o dof");
		expectedValue = objScan.reDouble("expectedValue o p");
		
		/* Realizar el ciclo de GausEquation para obtener un upperLimit satisfactorio. */
		upperLimit = objGaus.callSimpsonRule(numSeg, maxError, upperLimit, adjustment, degreeFreedom, expectedValue, widthSeg);
		
		/* Enviar el resultado a Output. */
		objOutput.writeData("outCurrentTest.txt", expectedValue, upperLimit);
	}
}